var hx = require("hbuilderx");

const { youdao } = require('translation.js')

//语种
const lang = 'zh-CN'

//该方法将在插件激活的时候调用
function activate(context) {
	let config = hx.workspace.getConfiguration("translation")
	let source = youdao
	let modeD = false
	let disposable = hx.commands.registerTextEditorCommand('extension.quicktranslation', async () => {
		 let word = await selectedText() 
		if (word.length > 0) {
			hx.window.setStatusBarMessage('正在积极翻译中，请等待！');
				// 翻译
			let bb = await trans(word, lang)
			hx.window.setStatusBarMessage(bb);
		} else {
			hx.window.showErrorMessage('当前未选中任何内容！');
		}
	});
	
	//订阅销毁钩子，插件禁用的时候，自动注销该command。
	context.subscriptions.push(disposable);

	//翻译函数
	async function trans(text, to, fm = null) {
		//检测语种
		if (fm === null) fm = await checklang(text)
		
		return new Promise(function(resolve, reject) {
			if (fm != to) {
				source.translate({
					text: text,
					from: fm,
					to: to
				}).then(result => {
					resolve(result.result[0])
				}).catch(err => {
					let mes = "异常错误"
					if (err.code === 'NETWORK_ERROR') mes = '网络错误'
					if (err.code === 'API_SERVER_ERROR') mes = '翻译接口返回了错误的数据'
					if (err.code === 'UNSUPPORTED_LANG') mes = '接口语种发生错误'
					if (err.code === 'NETWORK_TIMEOUT') mes = '接口超时了'
					hx.window.showErrorMessage(mes);
					reject(false)
				})
			} else {
				resolve(text)
			}
		})
	}

	//检测语种
	function checklang(text) {
		return new Promise(function(resolve, reject) {
			source.detect(text).then(ang => {
				resolve(ang)
			}).catch(err => {
				console.log(err)
				hx.window.showErrorMessage('检测文本语种失败！');
				reject(false)
			})
		})
	}
	
}
// 获取选中文本
async function selectedText(){
	//获取TextEditor：文本编辑器对象
	let editor = await hx.window.getActiveTextEditor();
	let selection = editor.selection;
	//当前选择内容
	return editor.document.getText(selection);
}

//该方法将在插件禁用的时候调用（目前是在插件卸载的时候触发）
function deactivate() {
}

module.exports = {
	activate,
	deactivate
}
